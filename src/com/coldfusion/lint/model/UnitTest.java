package com.coldfusion.lint.model;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;

public class UnitTest {
    public String getUnitTestRawResults(String testUrl) throws IOException {
        return IOUtils.toString(new URL(testUrl));
    }
}
