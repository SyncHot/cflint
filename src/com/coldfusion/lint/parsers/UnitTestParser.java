package com.coldfusion.lint.parsers;

import com.coldfusion.lint.actions.Redirector;

public class UnitTestParser {

    public static String getGlobalStats(String data, String urlRedirect) {
        String result = Parser.findPhrase(data, "<title>.*</title>");
        String errors = Parser.findPhrase(result, "Errors: [1-9]+|Fail: [1-9]+");

        result = result.replace("<title>", "<h1>");

        if (errors.length() > 0) {
            Redirector.redirectToPage(urlRedirect);
        }

        return result.replace("</title>", "</h1>");
    }

    public static String getUrl(String filePath, String userHome) {
        String path = "";
        if (filePath.contains("/modules/")) {
            try {
                path = Parser.findPhrase(filePath, "/modules/[A-z_]+");
                path = path.replace("/", ".");
                path = "http://" + userHome + "-sandbox.stepstone.de/modules/test/runner.cfm?directory=" + path + ".tests.unit&reporter=simple&fwreinit=true";
            } catch (Exception e) {
            }
        } else if (filePath.contains("/5/")) {
            try {
                path = Parser.findPhrase(filePath, "/5/[A-z_]+");
                path = path.replace("/", ".");
                path = path.replace("5.", "");
                path = "http://" + userHome + "-sandbox.stepstone.de/5/tests/runner.cfm?directory=5.tests.unit" + path + "&reporter=simple&fwreinit=true";
            } catch (Exception e) {
            }
        } else if (filePath.contains("/api/")) {
            try {
                path = Parser.findPhrase(filePath, "/api/[A-z_]+");
                path = path.replace("/", ".");
                path = path.replace("api.", "");
                path = "http://" + userHome + "-sandbox.stepstone.de/api/test/runner.cfm?directory=api.test.unit" + path + "&reporter=simple&fwreinit=true";
            } catch (Exception e) {
            }
        }

        return path;
    }
}
