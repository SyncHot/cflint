package com.coldfusion.lint.actions;

import java.awt.*;
import java.net.URI;

public class Redirector {

    public static void redirectToPage(String url) {

        if (Desktop.isDesktopSupported())

        {
            try {
                Desktop.getDesktop().browse(new URI(url));
            } catch (Exception e) {
            }
        }
    }
}
