package com.coldfusion.lint.listeners;

import com.coldfusion.lint.LintAnalyzer;
import com.coldfusion.lint.model.UnitTest;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class UnitTestsButtonActionListener implements ActionListener{

    private UnitTest unitTestModel;

    private LintAnalyzer lintAnalyzer;

    public UnitTestsButtonActionListener(LintAnalyzer lintAnalyzer){
        this.lintAnalyzer = lintAnalyzer;
    }

    public void actionPerformed(ActionEvent e) {
        this.lintAnalyzer.runUnitTest();
    }
}
